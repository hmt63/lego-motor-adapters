// stepper mount adapter
// adapted from https://3dprinting.stackexchange.com/questions/19522/stepper-motor-d-shaft-to-lego-axle-adapter

module fillet_square(width, radius) {
    translate([radius - width / 2, radius - width / 2, 0])
        minkowski() {
            square(width - 2 * radius);
            circle(radius);
        }
}

module lego_to_stepper_shaft_adapter (stepper_diameter = 4) {
        
    outer_diameter = 8.5;   //Outer diameter of the adapter
    stepper_length = 8;    //Length of the stepper shaft
    
    //4mm shaft: d = 4, offset = 1.6
    //5mm shaft: d = 5, offset = 2
    //6mm shaft: d = 6, offset = 2.5
    
    stepper_d_offset = 1.6;
    if (stepper_diameter == 4 ) {
        stepper_d_offset = 1.6; //Offset from the center of the shaft to the plane of the D
    } else if (stepper_diameter == 5) {
        stepper_d_offset = 2; //Offset from the center of the shaft to the plane of the D
    } else if (stepper_diameter ==6) {
        stepper_d_offset = 2.5; //Offset from the center of the shaft to the plane of the D
    } 
     
    thickness_mid = 2;      //Thickness of the massive section between stepper and lego shafts
    
    lego_length = 5 * 1.6 ;           //Length of the lego shaft
    lego_diameter = 4.9;        //Outer diameter of the lego shaft
    lego_internal_width = 1.9;    //Width of the slots for the shaft
    lego_corner_radius = 0.5;
    
    cutout_size = lego_diameter;
    cutout_translate = cutout_size / 2 + lego_internal_width / 2;
    $fn = 128;               //Accuracy / resolution of circles
    eps = 0.01;
    //color(0,0.5)
    union(){
        linear_extrude(height = stepper_length + eps) {
            difference() {
                circle(d = outer_diameter);   
                difference() {
                    circle(d = stepper_diameter);
                    union () {
                        translate([0, stepper_d_offset + stepper_diameter / 2, 0]) {
                            square(size = stepper_diameter, center = true);
                        }
                        translate([0, - (stepper_d_offset + stepper_diameter / 2), 0]) {
                            square(size = stepper_diameter, center = true);
                        }
                    }
                }
            }
        }
        translate([0, 0, stepper_length]) {        
            linear_extrude(height = thickness_mid) {
                circle(d = outer_diameter);
            }
        }
        translate([0, 0, stepper_length + thickness_mid - eps]) {        
            linear_extrude(height = lego_length + eps) {
                difference() {
                    circle(d = outer_diameter);
                    difference() {
                        circle(d = lego_diameter);
                        translate([cutout_translate, cutout_translate, 0]) {
                            fillet_square(cutout_size, lego_corner_radius);
                        }
                        translate([cutout_translate, -cutout_translate, 0]) {
                            fillet_square(cutout_size, lego_corner_radius);
                        }
                        translate([-cutout_translate, cutout_translate, 0]) {
                            fillet_square(cutout_size, lego_corner_radius);
                        }
                        translate([-cutout_translate, -cutout_translate, 0]) {
                            fillet_square(cutout_size, lego_corner_radius);
                        }
                    }
                }
            }
        }
    }
} // end module
lego_to_stepper_shaft_adapter(stepper_diameter = 5); 

//