
// lego beam
lu = 1.6 ;// LEGO unit
legointerval = 5 * lu ; // Lego 2-hole sinterval

res = 50 ;
nb_of_holes = 4 ;
rounded = true;

module legohole(fn = 20) {
    percage = 0.5 * lu ; // in lu
    color ("DarkRed", 1.0) union () {
        cylinder (h= 5 * lu, d = 3 * lu, center = true, $fn = fn) ;
        //evasement du haut
        translate ([0,0,(2.5 - 0.25) * lu]) 
        cylinder (h = 0.5*lu + percage, d = 3.5 * lu, center = true,$fn = fn) ;
        // evasement du bas
        translate ([0,0,-(2.5-0.25)*lu - percage]) 
        cylinder (h = 0.5*lu +percage , d = 3.5 * lu, center = true,$fn = fn) ;
    }
}
//translate ([0,0,0]) legohole() ;

// lego beam
module legobeam(length, rounded = true, roundedleft = true, roundedright = true, fn = 20) {
    difference () {
        union () {
            difference () { // we will remove the holes
                hull () {
                        if ((rounded == true) && (roundedleft == true)) {
                            cylinder (h= 5*lu, d = 4.5*lu, center = true, $fn =fn );
                        } else {
                            //cube (5*lu,center = true );
                            translate ([-2* lu, 0,0]) 
                            cube ([1*lu, 4.5*lu, 5*lu],center = true);
                        }
                        if ((rounded == true) && (roundedright == true)) {
                            translate ([(length -1) * 5 * lu, 0, 0] ) cylinder (h= 5*lu, d = 4.5*lu, center = true, $fn = fn );
                        } else {
                            translate ([(length -1) * 5 * lu + 2 * lu, 0, 0] )                             cube ([1*lu, 4.5*lu, 5*lu],center = true);
                        }
                } // end hull
                union () {
                        rotate (90, [0,1,0] ) translate ([2.5*lu, 0,0 * lu])  cylinder (h= (length-1)*5* lu, d = 3.5 * lu, center = false, $fn = fn);
                        rotate (90, [0,1,0] ) translate ([-2.5*lu, 0,0 * lu])  cylinder (h= (length-1)*5* lu, d = 3.5 * lu, center = false, $fn = fn);
                }// end union 
            } // end difference (creusement trou)
            for (i = [0: length -1]) {
                translate ([ i * 5 * lu, 0 ,0 ] ) cylinder (h= 5 * lu, d = 4.5 * lu, center = true, $fn = fn) ; 
            } // end for
        } // end union
        for (i = [0: length -1]) {
            translate ([ i * 5 * lu, 0 ,0 ] ) legohole (fn= fn) ;
        } // end for
    }  // end difference (holes)

} // end module

legobeam (nb_of_holes, rounded = rounded , fn= res ) ;
// translate ([3* legointerval, 1 * legointerval, 0]) rotate(180, [0,0,1])legobeam (4, rounded = true , fn = res) ;
//translate ([0* legointerval, 2 * legointerval, 0]) rotate(90, [1,0,0])legobeam (4, rounded = true  ,fn = res) ;