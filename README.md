Lego mount for servos

Here are two lego parts designed for adapting servos an stepper motors to the Lego system.

* Support for servo Futaba S3003
* Support for Step motor 5V 28BYJ-48

![Futaba motor adapter](images/servo_scad_view.png)

![Stepper motor adapter](images/stepper_scad_view.png)

These other parts are adapted form existing models : 
 * Stepper shaft adapter : 
modified (shortened) from [towe original design](https://3dprinting.stackexchange.com/questions/19522/stepper-motor-d-shaft-to-lego-axle-adapter)
 * Servo shaft adapter : design from the web (lost reference)