use <HMT_Lego_Beam.scad>;

lu = 1.6 ;//
lui = 4 * lu ; // ??
legointerval = 5 * lu ;

module servo_futaba_s3003 (screw= true, fn = 25) {
    translate ([0,0,8+2.5/2] ) color ("LightBlue") union () {
        translate([0, -10, -35/2]) cube ([20, 40,35], center = true ) ;
        translate([0, 0, 3/2+2]) cylinder (d= 6, h=3 ,center=true,$fn = fn ) ;
        translate([0, 0, 2/2]) cylinder (d= 15, h=2 ,center=true,$fn = fn) ;
        translate ([0, -30-8/2,-8 ]) cube ([18, 8, 2.5], center = true ) ; 
        translate ([0, 10+8/2, -8 ]) cube ([18, 8, 2.5], center = true ) ; 
        if (screw == true) {
            holediameter = 4;
            translate ([5, 10+8/2, -12 ])cylinder ( d=holediameter, h = 20, center = true ,$fn = fn);
            translate ([-5, 10+8/2, -12 ])cylinder ( d=holediameter, h = 20, center = true ,$fn = fn);
            translate ([5, -30-8/2, -12 ])cylinder ( d=holediameter, h = 20, center = true ,$fn = fn);
            translate ([-5, -30-8/2, -12 ])cylinder ( d=holediameter, h = 20, center = true ,$fn = fn);
        } // end screw
    }// end unions
}

//servo_futaba_s3003();

module support_servo (fn = 25){
    epaisseur = 3 ;
    tolerance = 0.25 ; //0.1 ; 
    hauteur = 10 ;
    difference () {
        union () { // placeholder
            union  (){
                
                translate ([0,-30,0]) hull () {
                    translate ([-10,0,-hauteur/2]) cylinder (d= epaisseur*2, h = hauteur, center = true, $fn= fn );
                    translate ([10,0,-hauteur/2] ) cylinder(d=epaisseur*2, h = hauteur, center = true, $fn= fn );
                    translate ([-10,40,-hauteur/2] ) cylinder(d=epaisseur*2, h = hauteur, center = true, $fn= fn );
                    translate ([10,40,-hauteur/2] ) cylinder(d=epaisseur*2, h = hauteur, center = true, $fn= fn );
                    
                    translate ([5, 40+8/2, -hauteur/2 ])cylinder ( d=8+4, h = hauteur, center = true ,$fn = fn);
                    translate ([-5, 40+8/2, -hauteur/2 ])cylinder ( d=8+4, h = hauteur, center = true ,$fn = fn);
                    translate ([5, -8/2, -hauteur/2 ])cylinder ( d=8, h = hauteur, center = true ,$fn = fn);
                    translate ([-5, -8/2, -hauteur/2 ])cylinder ( d=8, h = hauteur, center = true ,$fn = fn);
                } // end hull
                translate ([-2 * legointerval, 0,-0.5 *legointerval]) legobeam (5, rounded = true, fn = fn);
                translate ([-2 * legointerval, -2 * legointerval,-0.5 *legointerval]) legobeam (5, rounded = true, fn = fn);
            } // end union
        } //end offset
        union () {
            excave = 6 ;
            hexcave = 4 ;
            dexcave = -3 - hexcave/2;
            servo_futaba_s3003();
            translate([0, -10, -35/2 +8+2.5/2]) cube ([20+ 2* tolerance, 40 + 2*tolerance,35], center = true );
            *translate ([4, 10+8/2, dexcave ])cylinder ( d=excave, h = hexcave, center = true ,$fn = fn);
            *translate ([-4, 10+8/2, dexcave ])cylinder ( d=excave, h = hexcave, center = true ,$fn = fn);
            *translate ([4, -30-8/2, dexcave])cylinder ( d=excave, h = hexcave, center = true ,$fn = fn);
            *translate ([-4, -30-8/2, dexcave ])cylinder ( d=excave, h = hexcave, center = true ,$fn = fn);
            // passage pour le cable
            translate([0,10+0,-hauteur/2 +1]) cylinder(d= 7,h =hauteur+4, center =true,,$fn = fn) ; 
            translate([0,10+3,-hauteur/2+1]) cylinder(d= 4,h =hauteur+4, center =true,,$fn = fn) ; 
        }

    }// end difference            
}
       

support_servo(fn = 50) ;
