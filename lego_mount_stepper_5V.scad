// version 2 :
// optimisation du code
// offset de l'axe moteur corrigé à +7,5 au lieu de +6,5

use <HMT_Lego_Beam.scad>;

// vertical distance form stpper axis to mount holes
offset_axe_stepper = 7.5 ; // 6.5 selon le plan, 7.5 en fait
// extra holes on the side (not recommended)
extra = true ;
// number of holes in vetical beam (>= 7)
vertical_length = 7 ;
// rendering resolution
resolution = 25 ;

module __Customizer_Limit__ () {}
// lego unit
lu = 1.6 ;//

lui = 4 * lu ; // ??
// lego interval (5*lego unit)
legointerval = 5 * lu ;

        
module motor () {
    color ("DodgerBlue", 1.0) union() {

        // corps
        translate( [0,0,-19/2]) cylinder (h = 19, d=28, center = true);
        // axe moteur
        translate( [0,offset_axe_stepper,10/2]) cylinder (h= 10, d = 5, center = true);
        // base axe
        translate( [0,offset_axe_stepper,1]) cylinder (h= 2, d = 7.8, center = true);
        
        // tenons
        translate( [35/2,0,4]) cylinder (h= 10, d = 4, center = true, $fn = 20);
        translate( [-35/2,0,4]) cylinder (h= 10, d = 4, center = true, $fn = 20);
        
        // bloc alim
        translate( [0,-4,-18/2]) cube ([18,18,18], center = true);
        translate( [0,-10,-18/2]) cube ([14.2,14.2,18], center = true);
        translate( [0,-10,-8/2]) cube ([14.2,40,8], center = true);
        // support tenons
        hull () {
            translate( [35/2,0,-4]) cylinder (h= 8, d = 7, center = true);
            translate( [-35/2,0,-4]) cylinder (h= 8, d = 7, center = true);
        }
        
    }
}
//translate ([0,-offset_axe_stepper,-25])  motor() ;



module stepper_mount (extras= true, vertical_length = 7 , fn= 25) {
    difference () {
        union () {
            // base plate
            translate( [0,0,-2*lu]) cube ([3.1 * legointerval, 3.1*legointerval,1*lu], center = true) ; 
           translate ([-2* legointerval, 2 * legointerval, 0 ]) legobeam (5, rounded = true, fn = fn);
            // tenons à 90°
            if (extras == true) {
                translate ([-3*legointerval , 1*legointerval, 0]) rotate (90, [1,0,0]) legobeam (1, roundedright = false,fn = fn);
                translate ([3*legointerval , 1*legointerval, 0]) rotate (90, [1,0,0]) legobeam (1, roundedleft = false, fn = fn);
                translate ([-2.5*legointerval , 1*legointerval, 0]) rotate (90, [1,0,0]) cube ([1*lu, 4.5*lu, 5*lu],center = true);
                translate ([2.5*legointerval , 1*legointerval, 0]) rotate (90, [1,0,0]) cube ([1*lu, 4.5*lu, 5*lu],center = true);
                translate ([-3*legointerval , -3*legointerval, 0]) rotate (90, [1,0,0]) legobeam (1, roundedright = false, fn = fn);
                translate ([3*legointerval , -3*legointerval, 0]) rotate (90, [1,0,0]) legobeam (1, roundedleft = false, fn = fn);
                translate ([-2.5*legointerval , -3*legointerval, 0]) rotate (90, [1,0,0]) cube ([1*lu, 4.5*lu, 5*lu],center = true);
                translate ([2.5*legointerval , -3*legointerval, 0]) rotate (90, [1,0,0]) cube ([1*lu, 4.5*lu, 5*lu],center = true);
            
            }
            
            translate ([-2 * legointerval, -2 * legointerval, 0 ]) legobeam (5,fn = fn);
            translate ([-2 * legointerval, -4 * legointerval, 0 ]) legobeam (5, rounded = true, fn = fn);
            
                      
            // vertical beam
            translate ([-2 * legointerval,  -1 * round (vertical_length/2) * legointerval, 0 ]) rotate (90, [0,0,1])
            legobeam (vertical_length, fn = fn);
            translate ([2 * legointerval,  -1 * round (vertical_length/2) * legointerval, 0 ]) rotate (90, [0,0,1]) 
            legobeam (vertical_length, fn =fn);
            // mount for stepper motor
            translate ([-35/2, -offset_axe_stepper,  -0.5 * legointerval])
            cylinder (h =5*lu, d= 1.3 * legointerval, $fn = fn) ;
            //translate ([-35/2 + 4, -offset_axe_stepper,  0* legointerval]) cube (size =[  1.5 * legointerval, 1.5 * legointerval,5*lu], center= true) ;
            
           
            translate ([35/2, -offset_axe_stepper, -0.5 * legointerval])
            cylinder (h =5*lu, d= 1.3 * legointerval, $fn = fn) ;
            *translate ([35/2 - 4, -offset_axe_stepper,  0* legointerval]) cube (size =[  1.5 * legointerval, 1.5 * legointerval,5*lu], center= true) ;
        } // end union
        union () {
            // make nice holes nearvreinforcement
            translate ([-2*legointerval,0*legointerval]) legohole () ;
            translate ([2*legointerval,0*legointerval]) legohole () ;
            // make hole for shaft
            translate( [0,0,-2*lu])cylinder (h = 12, d=15, center = true, $fn = fn);
            // stepper with mount
            #translate ([0,-offset_axe_stepper, -2.5* lu -0.4]) motor() ; // remove motor hole
        }
    }// end intersection
}
stepper_mount(extras= extra , vertical_length = vertical_length , fn= resolution) ;